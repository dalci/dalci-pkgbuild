
[//]: # (
            This actually is the most
            platform independent comment
)

## dalci pkg-builds

| Package| Description | Author | Source |
| :- | :- | :- | :- |
| | | | |
| **aaxtomp3** | Convert Audible's .aax filetype to MP3, FLAC, M4A, or OPUS|   Krumpet Pirate              | [github][krumpetpirate]   |
| **arch-zsh-config** | Zsh configuration from manjaro forked for all the cool kids         |   Christian Schendel          | [github][doppelhelix1]    |
| **ausweisapp2** | A software application that you install on your computer to use your national identity card or your electronic residence permit for online identification             |   Governikus GmbH & Co. KG    | [github][Governikus]      |
| **bibata-modern-ice-cursor** | Bibata Cursor Theme (Modern Ice only), including hyprcursor and Xcursor |   rtgiskard                   | [github][rtgiskard]       |
| **bluejay** | Bluetooth manager written in Qt                 |   Evan Maddock                | [github][EbonJaeger]      |
| **cantata-qt6** | Qt6 graphical client for Music Player Daemon (MPD), nullobsi fork             |   Citlali del Rey             | [github][nullobsi]        |
| **cubiomes-viewer** | An efficient graphical Minecraft seed finder and map viewer         |   Cubitect                    | [github][Cubitect]        |
| **dalci-keyring** | dalci GPG keyring           |   Christian Schendel          | [gitlab][dalci]           |
| **dalci-mirrorlist**  | dalci_repo mirrorlist to use with Pacman       |   Christian Schendel          | [gitlab][dalci]           |
| **dalci-scel** | dot-files for /etc/skel              |   Christian Schendel          | [github][doppelhelix]     |
| **downgrade** | Bash script for downgrading one or more packages (Arch Only)               |   Pat Brisbin                 | [github][pbrisbin]        |
| **fortune-mod-de** | Fortune Cookies in german, from the Debian project          |   Andreas Tille               | [salsa.debian.org][tille]           |
| **fortune-mod-stripped** | The Fortune Cookie Program from BSD games - without cookies    |   Shlomi Fish                 | [github][shlomif] (stripped version by [Christian Schendel][dalci])|
| **gnome-shell-extension-alphabetical-grid-extension** | Restore the alphabetical ordering of the app grid, removed in GNOME 3.38      |   Stuart Hayhurst   | [github][stuart-hayhurst] |
| **gnome-shell-extension-applications-overview-tooltip** | Shows a tooltip over applications icons on applications overview      |   Raphaël Rochet   | [github][raphael-rochet1] |
| **gnome-shell-extension-arch-update** | Update indicator for ArchLinux and Gnome-Shell      |   Raphaël Rochet   | [github][raphael-rochet2] |
| **gnome-shell-extension-autohide-battery** | GNOME Shell extension to hide battery icon, if battery is fully charged and AC is connected      |   Andrey Sitnik   | [github][ai] |
| **gnome-shell-extension-blur-my-shell** | Extension that adds a blur look to different parts of the GNOME Shell      |   Aurélien Hamy   | [github][aunetx] |
| **gnome-shell-extension-gnome-clipboard-history** | Gnome Clipboard History is a Gnome extension that saves what you've copied into an easily accessible, searchable history panel.      |   Alex Saveau   | [github][SUPERCILEX] |
| **gnome-shell-extension-hide-universal-access** | A GNOME Shell extension to hide Universal Access icon from the status bar.      |   Akatsuki Rui   | [github][akiirui] |
| **gnome-shell-extension-nightthemeswitcher** |Automatically toggle your light and dark themes variants       |   Romain Vigier   | [github][rmnvgr] |
| **grub-theme-arch** | Grub theme for Arch Linux      |   ahmedmoselhi   | [github][ahmedmoselhi] |
| **hardinfo2** | System Information and Benchmark for Linux Systems.      |   hardinfo2    | [github][hardinfo2] |
| **hyprevents** |       |   Filip Markovic   | [github][vilari-mickopf] |
| **hyprprop** |       |   Filip Markovic   | [github][vilari-mickopf2] |
| **hyprsysteminfo** |       |   Hypr Development   | [github][Hypr] |
| **klassy** |       |   Paul A McAuley   | [github][paulmcauley] |
| **Koi** |       |   William   | [github](https://github.com/baduhai/Koi) |
| **kwin-effects-forceblur** |       |   taj-ny   | [github](https://github.com/taj-ny/kwin-effects-forceblur) |
| **libpamac** |       |   Manjaro Linux   | [github](https://github.com/manjaro/libpamac) |
| **mediaelch** |       |   Daniel Kabel   | [github](https://github.com/Komet/MediaElch) |
| **menulibre** |       |   Sean Davis   | [github](https://github.com/bluesabre/menulibre) |
| **mousam** |       |   Amit Chaudhary   | [github](https://github.com/amit9838/mousam) |
| **nautilus-admin-gtk4** |       |   Ataberk Özen   | [github](https://github.com/MacTavishAO/nautilus-admin-gtk4) |
| **nautilus-copy-path** |       |   Christoforos Aslanov   | [github](https://github.com/chr314/nautilus-copy-path) |
| **nautilus-open-any-terminal** |       |   Felix Bühler   | [github][Stunkymonkey] |
| **notparadoxlauncher** |       |   Shu Saura   | [github][shusaura85] |
| **pamac** |       |   Manjaro Linux   | [github](https://github.com/manjaro/pamac) |
| **papirus-icon-theme** |       |   Papirus Development Team   | [github][PapirusDevelopmentTeam] |
| **phinger-cursors** |       |   Philipp Schaffrath   | [github][phisch] |
| **archupdate** |       |   bouteillerAlan   | [github](https://github.com/bouteillerAlan/archupdate) |
| **plymouth-theme-aregression** |       |   Yozachar   | [github][yozachar] |
| **qt6ct-kde** |       |   ilya-fedin   | [github](https://github.com/ilya-fedin/qt6ct) |
| **railway** |       |   Schmiddi on Mobile   | [gitlab](https://gitlab.com/schmiddi-on-mobile/railway) |
| **signal-monochrome-tray** |       |   Christian Schendel   | [github][doppelhelix2] |
| **snapper-support** |       |   Garuda Linux   | [gitlab][snapper-support] |
| **termux-language-server** |       |   Termux   | [github][termux] |
| **thinlinc-client** |       |   ThinLinc   | [github][thinlinc] |
| **thinlinc-server** |       |   ThinLinc   | [github][thinlinc] |
| **tigervnc-viewer** |       |   TigerVNC   | [github][TigerVNC] |
| **tumbler-folder-thumbnailer** |       |   Christian Schendel   | [github](https://github.com/doppelhelix/tumbler-folder-thumbnailer) |
| **universal-android-debloater** |       |   Universal-Debloater-Alliance   | [github][Universal-Debloater-Alliance] |
| **uwsm** |       |   Vladimir-csp   | [github](https://github.com/Vladimir-csp/uwsm) |
| **ventoy** |       |   Ventoy   | [github](https://github.com/ventoy/Ventoy) |
| **wasistlos** |       |   Enes Hecan   | [github](https://github.com/xeco23/WasIstLos) |
| **waybar** |       |   Alexis Rouillard   | [github][Alexays] |
| **waybar-module-pacman-updates** |       |   Saltaformajo   | [github][coffebar] |
| **wlogout** |       |   Haden Collins   | [github][ArtsyMacaw] |
| **yay** |       |   Jguer   | [github][Jguer] |
| **zen-browser** |       |   Zen Browser   | [github](https://github.com/zen-browser/desktop) |
| **zsh-theme-powerlevel10k** |       |   Roman Perepelitsa   | [github][romkatv2] |

[krumpetpirate]: https://github.com/KrumpetPirate/AAXtoMP3
[Governikus]: https://github.com/Governikus/AusweisApp
[rtgiskard]: https://github.com/rtgiskard/bibata_cursor
[EbonJaeger]: https://github.com/EbonJaeger/bluejay
[nullobsi]: https://github.com/nullobsi/cantata
[Cubitect]: https://github.com/Cubitect/cubiomes-viewer
[btrfs-assistant]: https://gitlab.com/btrfs-assistant
[GeopJr]: https://github.com/GeopJr/Collision
[dalci]: https://gitlab.com/dalci
[doppelhelix]: https://github.com/doppelhelix/dalci-skel
[doppelhelix1]: https://github.com/doppelhelix/arch-zsh-config
[doppelhelix2]: https://github.com/doppelhelix/signal-monochrome-tray-hook
[pbrisbin]: https://github.com/archlinux-downgrade/downgrade
[tille]: https://salsa.debian.org/debian/fortunes-de
[shlomif]: https://github.com/shlomif/fortune-mod
[stuart-hayhurst]: https://github.com/stuarthayhurst/alphabetical-grid-extension
[raphael-rochet1]: https://github.com/RaphaelRochet/applications-overview-tooltip
[raphael-rochet2]: https://github.com/RaphaelRochet/arch-update
[ai]: https://github.com/ai/autohide-battery
[aunetx]: https://github.com/aunetx/blur-my-shell
[SUPERCILEX]: https://github.com/SUPERCILEX/gnome-clipboard-history
[akiirui]: https://github.com/akiirui/hide-universal-access
[rmnvgr]: https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension
[ahmedmoselhi]: https://github.com/ahmedmoselhi/distro-grub-themes
[hardinfo2]: https://github.com/hardinfo2/hardinfo2
[vilari-mickopf]: https://github.com/vilari-mickopf/hyprevents
[vilari-mickopf2]: https://github.com/vilari-mickopf/hyprprop
[Hypr]: https://github.com/hyprwm/hyprsysteminfo
[paulmcauley]: https://github.com/paulmcauley/klassy
[taj-ny]: https://github.com/taj-ny/kwin-effects-forceblur
[manjaro]: https://github.com/manjaro/libpamac/
[Komet]: https://github.com/Komet/MediaElch
[bluesabre]: https://github.com/bluesabre/menulibre
[MacTavishAO]: https://github.com/MacTavishAO/nautilus-admin-gtk4
[chr314]: https://github.com/chr314/nautilus-copy-path
[Stunkymonkey]: https://github.com/Stunkymonkey/nautilus-open-any-terminal
[Nextcloud]: https://github.com/nextcloud/desktop
[shusaura85]: https://github.com/shusaura85/notparadoxlauncher
[PapirusDevelopmentTeam]: https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
[phisch]: https://github.com/phisch/phinger-cursors
[yozachar]: https://github.com/yozachar/plymouth-theme-aregression
[snapper-support]: https://gitlab.com/garuda-linux/packages/stable-pkgbuilds/snapper-support
[termux]: https://github.com/termux/termux-language-server
[thinlinc]: https://www.cendio.com/
[TigerVNC]: https://github.com/TigerVNC/tigervnc
[Universal-Debloater-Alliance]: https://github.com/Universal-Debloater-Alliance/universal-android-debloater-next-generation
[Alexays]: https://github.com/Alexays/Waybar
[coffebar]: https://github.com/coffebar/waybar-module-pacman-updates
[keshavbhatt]: https://github.com/keshavbhatt/whatsie
[ArtsyMacaw]: https://github.com/ArtsyMacaw/wlogout
[Jguer]: https://github.com/Jguer/yay
[romkatv2]: https://github.com/romkatv/powerlevel10k
